from flask import Flask, redirect, url_for, render_template , request , session , flash

app = Flask(__name__)
app.secret_key = "hello"

@app.route("/home")
def home():
    return render_template("index_block1.html")

@app.route("/dccp")
def dccp():
    return render_template("dccp.html")

@app.route("/ispp1_verifone")
def ispp1_verifone():
    return render_template("ispp1_verifone.html")

if __name__ == "__main__":
    app.run(debug=True)